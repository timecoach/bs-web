import store from '@/store'
import empty from 'is-empty'
async function loadAppData () {
  try {
    let uid = localStorage.getItem('uid')
    if (!empty(uid) && empty(store.state.user.currentUser)) {
      await store.dispatch('user/fetchCurrentUser')
    }
    // if (empty(store.state.category.categories)) {
    //   store.dispatch('category/fetchCategoryAll')
    // }
  } catch (err) {
    console.error('err: ', err)
  }
}
loadAppData()
