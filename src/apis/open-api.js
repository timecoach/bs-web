import axios from 'axios'

let openAPIAxios = axios.create({
  baseURL: 'https://sgisapi.kostat.go.kr/OpenAPI3',
  timeout: 3000
})
let accessTokenRetryCount = 5
let openAPIAccessToken = 'openAPIAccessToken'

openAPIAxios.interceptors.response.use((resp) => {
  /**
   * 1) 만료시간 : 4시간
   * 2-1) 만료코드 : -201
   * 2-2) 인증코드오류 : -401
   */
  let accessTokenExpireErrorCodes = [-201, -401]
  if (accessTokenExpireErrorCodes.includes(resp.data.errCd) && accessTokenRetryCount > 0) {
    accessTokenRetryCount--
    return openApi.requestAccessToken().then(() => {
      let originalRequest = resp.config
      originalRequest.params.accessToken = localStorage.getItem(openAPIAccessToken)
      return openAPIAxios(originalRequest)
    })
  }
  return resp
}, (error) => {
  return Promise.reject(error)
})

const openApi = {
  requestAccessToken () {
    return new Promise((resolve, reject) => {
      openAPIAxios.get(`/auth/authentication.json`, {
        params: {
          consumer_key: '1f6309a53df5402eb68a',
          consumer_secret: '9dbe67ff27ff405a877a'
        }
      }).then((resp) => {
        localStorage.setItem(openAPIAccessToken, resp.data.result.accessToken)
        resolve()
      }).catch((err) => {
        reject(err)
      })
    })
  },
  async getAddress (params) {
    if (!localStorage.getItem(openAPIAccessToken)) {
      await openApi.requestAccessToken()
    }
    return new Promise((resolve, reject) => {
      Object.assign(params, {
        accessToken: localStorage.getItem(openAPIAccessToken)
      })
      openAPIAxios.get(`/addr/stage.json`, {
        params
      }).then((resp) => {
        resolve({
          success: true,
          status: resp.data.errCd === 0 ? 200 : resp.data.errCd,
          message: 'success',
          addressList: resp.data.result
        })
      }).catch((err) => {
        console.log('err : ', err)
        reject(err)
      })
    })
  },
  async getSiAddress (params) {
    let result = await this.getAddress({})
    let siAddress = result.addressList
    for (let i = 0; i < siAddress.length; i++) {
      siAddress[i].addr_name = siAddress[i].addr_name.replace('특별시', '')
                                                     .replace('광역시', '')
                                                     .replace('특별자치시', '')
                                                     .replace('특별자치도', '')
    }
    return siAddress
  }
}
export default openApi
