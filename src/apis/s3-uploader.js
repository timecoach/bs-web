import axios from 'axios'
import errs from 'errs'
import awsApig from './aws-apig'
import validate from '@/utils/validate'
import empty from 'is-empty'
const StoragePath = {
  article: 'origin/article',
  profile: 'origin/profile',
  review: 'origin/review'
}
// 1]글 등록 이미지 업로드 변경
// 2]프로필 사진 등록
const s3Uploader = {
  async uploads (storageKey, files) {
    let length = files.length
    let results = []
    for (let i = 0; i < length; i++) {
      results.push(await this.upload(storageKey, files[i]))
    }
    return results
  },
  async upload (storageKey, file) {
    let path = StoragePath[storageKey]
    if (!path) {
      throw errs.create({ code: 'S3UloaderError', message: 'storageKey parameter empty !!' })
    }
    let options = {
      params: { storagePath: path }
    }
    let signedResponse = await awsApig.get('/s3/signed', options)
    let urlData = signedResponse.data
    let params = {
      headers: { 'Content-Type': 'image/jpeg' }
    }
    await axios.put(urlData.url, file, params)
    urlData.path = urlData.key.replace('origin/', '')
    return urlData
  },
  async delete (keys, time) {
    if (time) {
      /* TODO
        업로드 실패가 되면 업로드된 파일(origin, resize)을 제거해야 하는데
        아직 제거할 RESIZE파일이 생성이 안되어 2초 기다린 후(resize가 생성이 될때까지...)
        삭제해야 하기 때문에 timeout을 설정함.
      */
      let promise = new Promise((resolve, reject) => {
        setTimeout(() => {
          resolve()
        }, 2000)
      })
      await promise
    }
    if (empty(keys)) {
      return
    }
    let response = null
    if (typeof keys === 'string' || keys.length === 1) {
      let key = typeof keys === 'string' ? keys : keys[0]
      response = await awsApig.post('/s3/delete', JSON.stringify({ key: key })).catch(err => err.response)
    } else {
      response = await awsApig.post('/s3/deletes', JSON.stringify({ keys })).catch(err => err.response)
    }
    validate.checkErrorResponse(response)
  }
}

export default s3Uploader
