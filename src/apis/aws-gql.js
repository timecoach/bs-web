import awsApig from './aws-apig'
import { jsonToGraphQLQuery } from 'json-to-graphql-query'

const awsGQL = {
  post (query) {
    const graphqlQuery = jsonToGraphQLQuery(query)
    return awsApig.post('/graphql', JSON.stringify({ query: graphqlQuery }))
  },
  query (query) {
    return this.post({
      query: query
    })
  },
  mutation (query) {
    return this.post({
      mutation: query
    })
  }
}
export default awsGQL
