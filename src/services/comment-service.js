import { awsGQL } from '@/apis'
import validate from '@/utils/validate'

const commentService = {
  async addComment (newComment) {
    const params = {
      createComment: {
        __args: {
          ...newComment
        },
        affectedRows: true,
        insertId: true
      }
    }
    let response = await awsGQL.mutation(params).catch(err => err.response)
    validate.checkErrorResponse(response)
  },
  async removeComment (payload) {
    const params = {
      deleteComment: {
        __args: {
          ...payload
        },
        affectedRows: true
      }
    }
    let response = await awsGQL.mutation(params).catch(err => err.response)
    validate.checkErrorResponse(response)
  }
}
export default commentService
