import userService from './user-service'
import categoryService from './category-service'
import articleService from './article-service'
import shopService from './shop-service'
import orderService from './order-service'
import commentService from './comment-service'
import storeService from './store-service'
export {
  userService,
  categoryService,
  articleService,
  shopService,
  orderService,
  commentService,
  storeService
}
