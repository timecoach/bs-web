import { awsGQL } from '@/apis'
import validate from '@/utils/validate'
const shopService = {
  async getShops (params) {
    const query = {
      getShops: {
        shopSeq: true,
        shopNm: true,
        shopType: true,
        photo: true,
        managerNm: true,
        managerCel: true,
        monthVisitCount: true,
        zipCd: true,
        basicAddr: true,
        detailAddr: true,
        bCd: true,
        si: true,
        gu: true,
        dong: true,
        lat: true,
        lng: true,
        zones: true,
        memo: true,
        shopSt: true,
        regDt: true
      }
    }
    let response = await awsGQL.query(query).catch(err => err.response)
    validate.checkErrorResponse(response)
    return response.data.data.getShops
  }
}
export default shopService
