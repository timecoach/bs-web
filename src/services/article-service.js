import { awsGQL, s3Uploader } from '@/apis'
import validate from '@/utils/validate'
async function deleteFiles (results) {
  if (results.length > 0) {
    let deleteKeys = results.map((item) => {
      return item.key
    })
    await s3Uploader.delete(deleteKeys, 2000)
  }
}
function parseEditPhotos (photos = [], results, removePaths) {
  let uploadPaths = results.map((item) => {
    return item.path
  })
  if (!photos) {
    return uploadPaths
  }
  for (let i = 0; i < removePaths.length; i++) {
    let imgUrl = removePaths[i].replace('origin/', '')
    let index = photos.indexOf(imgUrl)
    if (index !== -1) {
      photos.splice(index, 1)
    }
  }
  return photos.concat(uploadPaths)
}

const articleService = {
  async addArticle (newArticle) {
    let results = []
    try {
      results = await s3Uploader.uploads('article', newArticle.uploadFiles)
      newArticle.photos = results.map((item) => {
        return item.path
      })
      delete newArticle.uploadFiles
      const params = {
        createArticle: {
          __args: {
            ...newArticle
          },
          affectedRows: true,
          insertId: true
        }
      }
      let response = await awsGQL.mutation(params).catch(err => err.response)
      validate.checkErrorResponse(response)
      return response.data.data.createArticle.insertId
    } catch (err) {
      await deleteFiles(results)
      throw err
    }
  },
  async updateArticle (editArticle) {
    let results = []
    try {
      let uploadFiles = editArticle.uploadFiles
      let removePaths = editArticle.removePaths
      delete editArticle.uploadFiles
      delete editArticle.removePaths
      results = await s3Uploader.uploads('article', uploadFiles)
      editArticle.photos = parseEditPhotos(editArticle.photos, results, removePaths)
      const params = {
        updateArticle: {
          __args: {
            ...editArticle
          },
          affectedRows: true
        }
      }
      let response = await awsGQL.mutation(params).catch(err => err.response)
      validate.checkErrorResponse(response)
      s3Uploader.delete(removePaths)
    } catch (err) {
      await deleteFiles(results)
      throw err
    }
  },
  async getArticles (params) {
    const query = {
      getArticles: {
        __args: {
          ...params
        },
        articleSeq: true,
        title: true,
        photo: true,
        cateCd: true,
        cateNm: true,
        cateKey: true,
        price: true,
        priceOffer: true,
        dueDt: true,
        si: true,
        gu: true,
        dong: true,
        addrCd: true,
        articleSt: true,
        sortDt: true,
        upCount: true,
        sellerNickname: true,
        companyNm: true
      }
    }
    console.log('service: ', params)
    let response = await awsGQL.query(query).catch(err => err.response)
    validate.checkErrorResponse(response)
    return response.data.data.getArticles
  },
  async getArticle (articleSeq) {
    let params = {
      getArticle: {
        __args: {
          articleSeq: parseInt(articleSeq)
        },
        articleSeq: true,
        title: true,
        photos: true,
        photo: true,
        contents: true,
        topCateKey: true,
        topCateCd: true,
        topCateNm: true,
        cateCd: true,
        cateNm: true,
        cateKey: true,
        si: true,
        gu: true,
        dong: true,
        price: true,
        priceOffer: true,
        dueDt: true,
        tel: true,
        btnLink: true,
        articleSt: true,
        sortDt: true,
        applyForm: true,
        siCd: true,
        guCd: true,
        addrCd: true,
        visible: true,
        delYn: true,
        sellerNickname: true,
        sellerPicture: true,
        sellerId: true,
        sellerJob: true
      }
    }
    let response = await awsGQL.query(params).catch(err => err.response)
    validate.checkErrorResponse(response)
    return response.data.data.getArticle
  },
  async deleteArticle (payload) {
    const params = {
      deleteArticle: {
        __args: {
          ...payload
        },
        affectedRows: true
      }
    }
    let response = await awsGQL.mutation(params).catch(err => err.response)
    validate.checkErrorResponse(response)
  }
}
export default articleService
