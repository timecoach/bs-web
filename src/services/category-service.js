import { awsGQL } from '@/apis'
import { validate } from '@/utils'

const categoryService = {
  async getCategoryAll () {
    const params = {
      getCategoryAll: {
        cateSeq: true,
        cateNm: true,
        cateDesc: true,
        cateKey: true,
        cateCd: true,
        useRole: true,
        depth: true,
        childrens: {
          cateSeq: true,
          cateNm: true,
          cateKey: true,
          cateCd: true,
          depth: true
        }
      }
    }
    let response = await awsGQL.query(params).catch(err => err.response)
    validate.checkErrorResponse(response)
    let categoryAll = response.data.data.getCategoryAll
    let length = categoryAll.length
    let categories = {}
    for (let i = 0; i < length; i++) {
      categories[categoryAll[i].cateCd] = categoryAll[i]
    }
    return categories
  }
}

export default categoryService
