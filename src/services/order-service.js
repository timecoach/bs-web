import { awsGQL } from '@/apis'
import validate from '@/utils/validate'

const orderService = {
  async addOrder (newOrder) {
    const params = {
      createOrder: {
        __args: {
          ...newOrder
        },
        affectedRows: true,
        insertId: true
      }
    }
    let response = await awsGQL.mutation(params).catch(err => err.response)
    validate.checkErrorResponse(response)
    return response.data.data.createOrder.insertId
  },
  async getOrders (payload) {
    const query = {
      getOrders: {
        __args: {
          ...payload
        },
        orderSeq: true,
        orderSt: true,
        orderDt: true,
        orderArticleSeq: true,
        orderDeliverySeq: true,
        receiverNm: true,
        receiverPhone: true,
        nickname: true,
        picture: true,
        job: true,
        articleSeq: true,
        title: true,
        photo: true,
        upCount: true,
        cateCd: true,
        si: true,
        gu: true,
        dong: true,
        price: true,
        dueDt: true,
        articleSt: true,
        sortDt: true,
        regDt: true
      }
    }
    let response = await awsGQL.query(query).catch(err => err.response)
    validate.checkErrorResponse(response)
    return response.data.data.getOrders
  },
  async getOrder (payload) {
    const query = {
      getOrder: {
        __args: {
          ...payload
        },
        orderSeq: true,
        orderSt: true,
        orderDt: true,
        orderArticleSeq: true,
        orderDeliverySeq: true,
        receiverNm: true,
        receiverPhone: true,
        basicAddr: true,
        detailAddr: true,
        memo: true,
        articleSeq: true,
        regDt: true
      }
    }
    let response = await awsGQL.query(query).catch(err => err.response)
    validate.checkErrorResponse(response)
    return response.data.data.getOrder
  }
}
export default orderService
