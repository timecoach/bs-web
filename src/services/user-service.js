import { Auth } from 'aws-amplify'
import { awsGQL, s3Uploader } from '@/apis'
import { validate } from '@/utils'
import camelcaseKeys from 'camelcase-keys'
import empty from 'is-empty'

function parseAttributesCurrentUser (currentUser) {
  if (!currentUser.attributes) {
    return currentUser
  }
  if (currentUser.attributes['custom:job']) {
    let job = currentUser.attributes['custom:job']
    delete currentUser.attributes['custom:job']
    currentUser.attributes.job = job
  }
  if (currentUser.attributes['custom:role']) {
    let role = currentUser.attributes['custom:role']
    delete currentUser.attributes['custom:role']
    currentUser.attributes.role = role
  }

  if (currentUser.attributes['phone_number']) {
    let phoneNumber = currentUser.attributes['phone_number']
    currentUser.attributes['phone_number_v2'] = phoneNumber.slice(3)
  }
  return camelcaseKeys(currentUser.attributes)
}
async function updateCognitoUser (user) {
  let currentUser = await Auth.currentAuthenticatedUser()
  let cognitoUserArrtibutes = {
    'gender': user.gender,
    'custom:job': user.job,
    'nickname': user.nickname,
    'birthdate': user.birthdate,
    'phone_number': user.phoneNumber,
    'picture': !user.picture ? '' : user.picture,
    'custom:role': !user.role ? 'member' : user.role
  }
  return Auth.updateUserAttributes(currentUser, cognitoUserArrtibutes)
}
function updateDatabaseUser (user) {
  const params = {
    updateUser: {
      __args: {
        uid: user.uid,
        job: user.job,
        gender: user.gender,
        nickname: user.nickname,
        birthdate: user.birthdate,
        phoneNumber: user.phoneNumber,
        role: !user.role ? 'member' : user.role,
        picture: !user.picture ? null : user.picture
      },
      affectedRows: true
    }
  }
  return awsGQL.mutation(params).catch(err => err.response)
}
const userService = {
  async createUser (payload) {
    let role = 'member'
    let cognitoUserArrtibutes = {
      'custom:role': role,
      'birthdate': payload.birthdate,
      'gender': payload.gender,
      'custom:job': payload.job,
      'nickname': payload.nickname,
      'phone_number': payload.phoneNumber
    }
    if (payload.picture) {
      cognitoUserArrtibutes.picture = payload.picture
    }
    let result = await Auth.signUp({
      username: payload.username,
      password: payload.password,
      attributes: {
        ...cognitoUserArrtibutes
      }
    })
    if (result.userSub) {
      const params = {
        createUser: {
          __args: {
            role: role,
            job: payload.job,
            uid: result.userSub,
            birthdate: payload.birthdate,
            gender: payload.gender,
            nickname: payload.nickname,
            phoneNumber: payload.phoneNumber,
            picture: !payload.picture ? null : payload.picture
          },
          affectedRows: true
        }
      }
      let response = await awsGQL.mutation(params).catch(err => err.response)
      validate.checkErrorResponse(response)
    }
    return result
  },
  async getCurrentUser () {
    try {
      let currentUser = await Auth.currentAuthenticatedUser()
      currentUser = parseAttributesCurrentUser(currentUser)
      return currentUser
    } catch (err) {
      if (err === 'not authenticated') {
        return null
      } else {
        alert(err)
      }
    }
  },
  async updateUser (user) {
    let result = null
    try {
      if (!empty(user.newFile)) {
        result = await s3Uploader.upload('profile', user.newFile)
        user.picture = result.path
      }
      let response = await updateDatabaseUser(user)
      validate.checkErrorResponse(response)
      await updateCognitoUser(user)
      if (user.removePath) {
        s3Uploader.delete(user.removePath)
      }
    } catch (err) {
      if (result) {
        await s3Uploader.delete(result.key, 1000)
      }
      throw err
    }
  }
}
export default userService
