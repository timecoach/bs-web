import { mapGetters } from 'vuex'
import empty from 'is-empty'
const GlobalMixins = {
  install (Vue) {
    Vue.mixin({
      data: () => ({
        genders: [
          { text: '남자', value: 'MALE' },
          { text: '여자', value: 'FEMALE' }
        ],
        jobs: [
          { text: '직업을 선택해주세요', value: null },
          { text: '개인카페', value: '개인카페' },
          { text: '카페매니저', value: '카페매니저' },
          { text: '프차카페', value: '프차카페' },
          { text: '카페알바', value: '카페알바' }
        ],
        articleSts: {
          sell: '판매중',
          booking: '예약중',
          complete: '판매완료'
        },
        resizeImgUrl: `${process.env.IMAGE_BASE_URL}/resize/`
      }),
      computed: {
        ...mapGetters(['isLogin', 'currentUser'])
      },
      created () {
      },
      methods: {
        demo () {
          alert('준비중입니다 :)')
        },
        scrollToTop () {
          document.body.scrollTop = 0 // safari
          document.documentElement.scrollTop = 0 // anyway
        },
        image (type, size, photo) {
          if (!empty(photo)) {
            return `${this.resizeImgUrl}${size}/${photo}`
          } else {
            return require(`@/assets/default/${type}_${size}.png`)
          }
        },
        handleError (err) {
          console.log('handleError: ', err)
          let errCode = err.code
          if (!errCode) {
            alert(err)
            return
          }
          let message = ''
          switch (errCode) {
            case 'UsernameExistsException': message = '이미 가입한 휴대전화 번호입니다'; break
            default: message = err.message
          }
          console.log(`code: ${errCode}, message: ${err.message}`)
          alert(message)
        },
        getSubCategories (cateKey, prependCate) {
          let currentCategory = this.$store.state.category.categories[cateKey]
          if (currentCategory) {
            if (prependCate) {
              return [prependCate].concat(currentCategory.childrens)
            }
            return currentCategory.childrens
          } else {
            return {}
          }
        },
        validateState (ref) {
          if (this.veeFields[ref] && (this.veeFields[ref].dirty || this.veeFields[ref].validated)) {
            return !this.errors.has(ref)
          }
          return null
        }
      }
    })
  }
}

export default GlobalMixins
