import BootstrapVue from 'bootstrap-vue'
import GlobalMixins from './global-mixins'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import '@/styles/common.scss'
import '@/styles/web.scss'
import './vue-croppa'
import './vuex'
import './vue-moment'
import './vue-numeric'
import './vue-meta'
import './vee-validate'
import './v-mask'

// import './vue-extend-layout'

export default {
  install (Vue) {
    Vue.use(GlobalMixins)
    Vue.use(BootstrapVue)
  }
}
