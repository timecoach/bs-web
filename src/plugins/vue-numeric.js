import Vue from 'vue'
/*
설명 : 숫자 포맷
Docs : https://www.npmjs.com/package/vue-numeric
*/
import VueNumeric from 'vue-numeric'
Vue.use(VueNumeric)
