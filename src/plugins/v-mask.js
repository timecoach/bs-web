import Vue from 'vue'
/*
설명 : 데이터 패턴
Docs : https://www.npmjs.com/package/v-mask
*/
import VueMask from 'v-mask'
Vue.use(VueMask)
