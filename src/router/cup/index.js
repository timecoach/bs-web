import MainNavbar from '@/layout/cup/MainNavbar'
import MainFooter from '@/layout/cup/MainFooter'
import Home from '@/views/cup/Home'
import SearchMap from '@/views/cup/SearchMap'
import Price from '@/views/cup/Price'
import Campaign from '@/views/cup/Campaign'
import Introduce from '@/views/cup/Introduce'

let defaultMeta = {
  layout: 'cup/default'
}
let cupRouter = [
  {
    path: '/cup',
    name: 'CupHome',
    components: { default: Home, header: MainNavbar, footer: MainFooter },
    meta: defaultMeta
  },
  {
    path: '/cup/search',
    name: 'SearchMap',
    components: { default: SearchMap, header: MainNavbar },
    meta: defaultMeta
  },
  {
    path: '/cup/introduce',
    name: 'Introduce',
    components: { default: Introduce, header: MainNavbar, footer: MainFooter },
    meta: defaultMeta
  },
  {
    path: '/cup/price',
    name: 'Price',
    components: { default: Price, header: MainNavbar, footer: MainFooter },
    meta: defaultMeta
  },
  {
    path: '/cup/campaigns',
    name: 'Campaign',
    components: { default: Campaign, header: MainNavbar, footer: MainFooter },
    meta: defaultMeta
  }
]
export default cupRouter
