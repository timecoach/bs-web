import Vue from 'vue'

import Router from 'vue-router'
import Home from '@/views/Home'
import SignUp from '@/views/SignUp'
import Login from '@/views/Login'
import ArticleSearchGoods from '@/views/article/ArticleSearchGoods'
import ArticleSearchFree from '@/views/article/ArticleSearchFree'
import ArticleSearchExhibition from '@/views/article/ArticleSearchExhibition'
import ArticleSearchInterior from '@/views/article/ArticleSearchInterior'
import ArticleSearchEducation from '@/views/article/ArticleSearchEducation'
import ArticleSearchRecipe from '@/views/article/ArticleSearchRecipe'
import ArticleView from '@/views/article/ArticleView'
import ArticleCategorySelection from '@/views/article/ArticleCategorySelection'
import ArticleAction from '@/views/article/ArticleAction'
import ArticleManage from '@/views/article/ArticleManage'
import ArticleSubmitList from '@/views/article/ArticleSubmitList'
import ArticleSubmitForm from '@/views/article/ArticleSubmitForm'
import OrderTest from '@/views/OrderTest'
import Profile from '@/views/Profile'
import MainNavbar from '@/layout/MainNavbar'
import LogoNavbar from '@/layout/LogoNavbar'
import ManageHeader from '@/layout/ManageHeader'
import LoginNavbar from '@/layout/LoginNavbar'
import MainFooter from '@/layout/MainFooter'
import cupRouter from './cup'

Vue.use(Router)

let router = new Router({
  mode: 'history',
  routes: [
    ...cupRouter,
    {
      path: '/',
      name: 'Home',
      components: { default: Home, header: MainNavbar, footer: MainFooter }
    },
    {
      path: '/order',
      name: 'OrderTest',
      components: { default: OrderTest, header: MainNavbar, footer: MainFooter }
    },
    {
      path: '/signup',
      name: 'SignUp',
      components: { default: SignUp, header: LoginNavbar }
    },
    {
      path: '/login',
      name: 'Login',
      components: { default: Login, header: LoginNavbar }
    },
    {
      path: '/goods/:topCateCd',
      name: 'ArticleSearchGoods',
      props: { name: 'world' },
      components: { default: ArticleSearchGoods, header: MainNavbar, footer: MainFooter }
    },
    {
      path: '/free/:topCateCd',
      name: 'ArticleSearchFree',
      components: { default: ArticleSearchFree, header: MainNavbar, footer: MainFooter }
    },
    {
      path: '/exhibition/:topCateCd',
      name: 'ArticleSearchExhibition',
      props: { topCateNm: '교육' },
      components: { default: ArticleSearchExhibition, header: MainNavbar, footer: MainFooter }
    },
    {
      path: '/interior/:topCateCd',
      name: 'ArticleSearchInterior',
      components: { default: ArticleSearchInterior, header: MainNavbar, footer: MainFooter }
    },
    {
      path: '/education/:topCateCd',
      name: 'ArticleSearchEducation',
      components: { default: ArticleSearchEducation, header: MainNavbar, footer: MainFooter }
    },
    {
      path: '/recipe/:topCateCd',
      name: 'ArticleSearchRecipe',
      components: { default: ArticleSearchRecipe, header: MainNavbar, footer: MainFooter }
    },
    {
      path: '/articles/manage',
      name: 'ArticleManage',
      components: { default: ArticleManage, header: MainNavbar, footer: MainFooter, subHeader: ManageHeader }
    },
    {
      path: '/articles/:articleSeq/submit',
      name: 'ArticleSubmitList',
      components: { default: ArticleSubmitList, header: MainNavbar, footer: MainFooter }
    },
    {
      path: '/articles/submit',
      name: 'ArticleSubmitForm',
      components: { default: ArticleSubmitForm, header: LogoNavbar }
    },
    {
      path: '/category/selection',
      name: 'ArticleCategorySelection',
      components: { default: ArticleCategorySelection, header: MainNavbar, footer: MainFooter }
    },
    {
      path: '/articles/create/:cateCd',
      name: 'ArticleCreate',
      components: { default: ArticleAction, header: MainNavbar, footer: MainFooter }
    },
    {
      path: '/articles/edit/:articleSeq',
      name: 'ArticleEdit',
      components: { default: ArticleAction, header: MainNavbar, footer: MainFooter }
    },
    {
      path: '/articles/:articleSeq',
      name: 'ArticleView',
      components: { default: ArticleView, header: MainNavbar, footer: MainFooter }
    },
    {
      path: '/profile',
      name: 'Profile',
      components: { default: Profile, header: MainNavbar, footer: MainFooter, subHeader: ManageHeader }
    }
  ]
})
router.beforeEach(async (to, from, next) => {
  let checkPath = true
  if ((checkPath = checkSubdomainPath(to)) !== true) {
    next(checkPath)
    return
  }
  next()
})
function checkSubdomainPath (to) {
  let subdomain = location.hostname.split('.')[0]
  let toPath = to.path
  let domains = ['cup', 'm', 'admin']
  if (domains.indexOf(subdomain) !== -1 && toPath.startsWith(`/${subdomain}`) === false) {
    return `/cup${toPath}`
  }
  return true
}
export default router
