import { getField } from 'vuex-map-fields'
import empty from 'is-empty'
const getters = {
  getField,
  isLogin (state) {
    if (empty(state.user.currentUser)) {
      return false
    } else {
      return true
    }
  },
  currentUser (state) {
    if (empty(state.user.currentUser)) {
      return {}
    } else {
      return state.user.currentUser
    }
  }
}
export default getters
