import { categoryService } from '@/services'

const state = {
  currentCate: {},
  categories: []
}
const getters = {
  currentCateNm (state) {
    if (state.currentCate) {
      return state.currentCate.cateNm
    }
    return ''
  },
  currentCateCd (state) {
    if (state.currentCate) {
      return state.currentCate.cateCd
    }
    return ''
  },
  currentCateKey (state) {
    if (state.currentCate) {
      return state.currentCate.cateKey
    }
    return ''
  }
}
const mutations = {
  setCategories (state, categories) {
    state.categories = categories
  },
  setCurrentCate (state, cate) {
    state.currentCate = cate
  }
}
const actions = {
  async fetchCategoryAll (context, payload) {
    let categories = await categoryService.getCategoryAll()
    context.commit('setCategories', categories)
  }
}
export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
