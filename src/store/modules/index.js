import user from './user'
import category from './category'
import article from './article'
import shop from './shop'
import order from './order'
import comment from './comment'
import store from './store'
export default {
  user,
  category,
  article,
  shop,
  order,
  comment,
  store
}
