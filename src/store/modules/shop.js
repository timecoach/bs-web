import { shopService } from '@/services'

const state = {
}
const getters = {
}
const mutations = {
}
const actions = {
  async getShops (context, payload) {
    let shops = await shopService.getShops()
    return shops
  }
}
export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
