import { storeService } from '@/services'

const state = {
}
const getters = {
}
const mutations = {
}
const actions = {
  async getStores (context, payload) {
    let stores = await storeService.getStores()
    return stores
  }
}
export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
