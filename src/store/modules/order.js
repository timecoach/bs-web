import { orderService } from '@/services'
const state = {
  orderArticles: [],
  orderArticlesMoreEnd: false,
  articleOrders: [],
  articleOrdersMoreEnd: false
}

const getters = {
}

const mutations = {
  setOrderArticles (state, orders) {
    state.orderArticles = orders
  },
  appendOrderArticles (state, orders) {
    state.orderArticles = state.orderArticles.concat(orders)
  },
  setOrderArticlesMoreEnd (state, moreEnd) {
    state.orderArticlesMoreEnd = moreEnd
  },
  setArticleOrders (state, orders) {
    state.articleOrders = orders
  },
  appendArticleOrders (state, orders) {
    state.articleOrders = state.articleOrders.concat(orders)
  },
  setArticleOrdersMoreEnd (state, moreEnd) {
    state.articleOrdersMoreEnd = moreEnd
  }
}

const actions = {
  async addOrder (context, payload) {
    let orderSeq = await orderService.addOrder(payload)
    console.log('orderSeq : ', orderSeq)
  },
  async fetchOrderArticles (context, payload) {
    let params = Object.assign({
      offset: 0,
      limit: 40
    }, payload)
    let result = await context.dispatch('getOrders', params)
    console.log('result : ', result)
    if (params.offset === 0) {
      context.commit('setOrderArticles', result.orders)
    } else {
      context.commit('appendOrderArticles', result.orders)
    }
    context.commit('setOrderArticlesMoreEnd', result.moreEnd)
  },
  async fetchArticleOrders (context, payload) {
    let params = Object.assign({
      offset: 0,
      limit: 40
    }, payload)
    let result = await context.dispatch('getOrders', params)
    console.log('result : ', result)
    if (params.offset === 0) {
      context.commit('setArticleOrders', result.orders)
    } else {
      context.commit('appendArticleOrders', result.orders)
    }
    context.commit('setArticleOrdersMoreEnd', result.moreEnd)
  },
  async getOrders (context, payload) {
    let params = Object.assign({
      offset: 0,
      limit: 40
    }, payload)
    let orders = await orderService.getOrders(params)
    return {
      orders: orders,
      moreEnd: orders.length < params.limit
    }
  },
  async getOrder (context, payload) {
    let order = await orderService.getOrder(payload)
    return order
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
