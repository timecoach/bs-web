import { commentService } from '@/services'
const state = {
}

const getters = {
}

const mutations = {
}

const actions = {
  async addComment (context, payload) {
    await commentService.addComment(payload)
  },
  async removeComment (context, payload) {
    await commentService.removeComment(payload)
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
