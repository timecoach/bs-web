import { userService } from '@/services'
import { Auth } from 'aws-amplify'

const state = {
  currentUser: {}
}
const getters = {
}
const mutations = {
  setCurrentUser (state, currentUser) {
    state.currentUser = currentUser
  }
}
const actions = {
  async signUp (context, payload) {
    await userService.createUser(payload)
  },
  async login (context, payload) {
    let result = await Auth.signIn(payload.username, payload.password)
    localStorage.setItem('uid', result.username)
    await context.dispatch('fetchCurrentUser')
  },
  async fetchCurrentUser (context) {
    let currentUser = await userService.getCurrentUser()
    if (currentUser) {
      await context.commit('setCurrentUser', currentUser)
    }
  },
  async logout (context) {
    await Auth.signOut({ global: true }) // facebook, cognito, credentials
    localStorage.removeItem('uid')
    context.commit('setCurrentUser', {})
  },
  async saveUser (context, user) {
    let result = await userService.updateUser(user)
    await context.dispatch('fetchCurrentUser')
    return result
  }
}
export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
