import { articleService } from '@/services'
const state = {
  articles: [],
  moreEnd: false
}

const getters = {
}

const mutations = {
  setArticles (state, articles) {
    state.articles = articles
  },
  appendArticles (state, articles) {
    state.articles = state.articles.concat(articles)
  },
  setMoreEnd (state, moreEnd) {
    state.moreEnd = moreEnd
  }
}

const actions = {
  async addArticle (context, payload) {
    let articleSeq = await articleService.addArticle(payload)
    console.log('articleSeq : ', articleSeq)
  },
  async updateArticle (context, payload) {
    await articleService.updateArticle(payload)
  },
  async getArticle (context, articleSeq) {
    let article = await articleService.getArticle(articleSeq)
    return article
  },
  async getMyArticles (context, payload) {
    let result = await context.dispatch('getArticles', payload)
    return result
  },
  async getArticles (context, payload) {
    let params = Object.assign({
      offset: 0,
      limit: 40
    }, payload)
    let articles = await articleService.getArticles(params)
    return {
      articles: articles,
      moreEnd: articles.length < params.limit
    }
  },
  async fetchArticles (context, payload) {
    let params = Object.assign({
      offset: 0,
      limit: 40,
      cateCd: this.getters['category/currentCateCd']
    }, payload)
    console.log('paramsparamsparams: ', params)
    let result = await context.dispatch('getArticles', params)
    if (params.offset === 0) {
      context.commit('setArticles', result.articles)
    } else {
      context.commit('appendArticles', result.articles)
    }
    context.commit('setMoreEnd', result.moreEnd)
    return params
  },
  async deleteArticle (context, payload) {
    await articleService.deleteArticle(payload)
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
