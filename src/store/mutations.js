import { updateField } from 'vuex-map-fields'
const mutations = {
  updateField
}

export default mutations
