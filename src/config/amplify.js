import Amplify from 'aws-amplify'
import awsConfig from './aws'
Amplify.configure({
  Auth: {
    identityPoolId: awsConfig.IdentityPoolId,
    region: awsConfig.region,
    userPoolId: awsConfig.UserPoolId,
    userPoolWebClientId: awsConfig.ClientId
  }
})
