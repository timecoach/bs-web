import errs from 'errs'
import empty from 'is-empty'
const validate = {
  checkErrorResponse (response) {
    if (!response.data) {
      console.error('response: ', response)
      return
    }
    let errors = response.data.errors
    if (!empty(errors)) {
      let error = errors[0]
      if (empty(error.extensions.code)) {
        throw errs.create({
          code: 'InternalServerError',
          message: error.message
        })
      } else {
        throw errs.create({
          code: error.extensions.code,
          message: error.message
        })
      }
    }
  },
  isEmpty (obj) {
    return empty(obj)
  }
}
export default validate
