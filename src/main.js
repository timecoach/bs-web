import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'
import WebUiKit from './plugins/web-ui-kit'
import '@/config'
import './load-app-data'

Vue.use(WebUiKit)
Vue.config.productionTip = false
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
