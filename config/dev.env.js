'use strict'

const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  IMAGE_BASE_URL: '"https://s3.ap-northeast-2.amazonaws.com/bs-dev-origin"'
})
